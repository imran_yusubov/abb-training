package az.abb.training.rest.dto;

import az.abb.training.domain.account.AccountEntity;
import lombok.Data;

import java.util.List;

@Data
public class CustomerDto {

    private Long id;

    private String firstName;

    private String lastName;

   // private List<AccountDto> accountEntities;
}
