package az.abb.training.rest.dto;

import az.abb.training.domain.account.AccountType;
import az.abb.training.domain.account.Currency;
import az.abb.training.domain.account.CustomerEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
public class AccountDto {

    private Long id;

    private String accountNumber;

    private AccountType accountType;

    private Double balance;

    private Currency currency;

}
