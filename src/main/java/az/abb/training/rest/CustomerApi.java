package az.abb.training.rest;

import az.abb.training.domain.account.CustomerEntity;
import az.abb.training.rest.dto.CustomerDto;
import az.abb.training.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
public class CustomerApi {

    private final CustomerService customerService;

    @GetMapping
    @SneakyThrows
    public List<CustomerDto> getCustomers() {
        final List<CustomerDto> customers = customerService.findAll();
        Thread.sleep(60_000);
        return customers;
    }

}
