package az.abb.training;

import az.abb.training.domain.account.AccountEntity;
import az.abb.training.domain.account.AccountType;
import az.abb.training.domain.account.Currency;
import az.abb.training.domain.account.CustomerEntity;
import az.abb.training.domain.account.CustomerRepository;
import az.abb.training.rest.dto.CustomerDto;
import az.abb.training.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@RequiredArgsConstructor
@SpringBootApplication
public class TrainingApplication implements CommandLineRunner {

    private final CustomerRepository customerRepository;
    private final CustomerService service;

    public static void main(String[] args) {
        SpringApplication.run(TrainingApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        //customer save -
        //for (int i = 0; i < 100; i++)
        //   customerRepository.save(createCustomer());

        // final List<CustomerDto> all = service.findAll();
        //System.out.println(all);

        Thread thread1 = new Thread(service::findAll);
        Thread thread2 = new Thread(service::findAll);
        Thread thread3 = new Thread(service::findAll);

        final long currentTimeMillis = System.currentTimeMillis();
        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();
        System.out.println("Duration " + (System.currentTimeMillis() - currentTimeMillis));
    }


    private CustomerEntity createCustomer() {
        CustomerEntity customer =
                CustomerEntity.builder()
                        .firstName("Vusal")
                        .lastName("Abdullayev")
                        .build();

        AccountEntity account1 =
                AccountEntity
                        .builder()
                        .accountType(AccountType.SAVING)
                        .accountNumber("123")
                        .balance(100D)
                        .customer(customer)
                        .currency(Currency.AZN)
                        .build();

        AccountEntity account2 =
                AccountEntity
                        .builder()
                        .accountType(AccountType.SAVING)
                        .accountNumber("124")
                        .balance(100D)
                        .customer(customer)
                        .currency(Currency.USD)
                        .build();

        customer.setAccountEntities(List.of(account1, account2));
        return customer;
    }
}
