package az.abb.training.service;

import az.abb.training.rest.dto.CustomerDto;

import java.util.List;

public interface CustomerService {
    List<CustomerDto> findAll();
}
