package az.abb.training.service;

import az.abb.training.domain.account.CustomerEntity;
import az.abb.training.domain.account.CustomerRepository;
import az.abb.training.rest.dto.CustomerDto;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {


    // @PersistenceContext
    private final EntityManager entityManager;

    private final EntityManagerFactory entityManagerFactory;
    private final CustomerRepository customerRepository;
    private final ModelMapper mapper;

    @Override
    //@Transactional
    @SneakyThrows
    public List<CustomerDto> findAll() {
        //
        //final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.createEntityGraph(CustomerEntity.class)
                .addAttributeNodes("accountEntities");
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        final CustomerEntity customer = entityManager.find(CustomerEntity.class, 2L);
        System.out.println("Entity manager " + entityManager);
        System.out.println(customer);
        Thread.sleep(30_000);
//        return customerRepository.findAllWithAccounts()
//                .stream()
//                .map(customerEntity -> mapper.map(customerEntity, CustomerDto.class))
//                .toList();

        transaction.commit();
        entityManager.close();
        return List.of(mapper.map(customer, CustomerDto.class));
    }
}
