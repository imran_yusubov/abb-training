package az.abb.training.domain.account;

public enum Currency {
    AZN, USD
}
