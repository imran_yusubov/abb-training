package az.abb.training.domain.account;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

//    @EntityGraph(attributePaths = CustomerEntity.Fields.accountEntities)
//    List<CustomerEntity> findAllWithAccounts();
}
