package az.abb.training.domain.account;

public enum AccountType {

    CURRENT, SAVING
}
